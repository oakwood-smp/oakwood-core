package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import org.bukkit.Location;
import org.bukkit.entity.Player;

@OakwoodCommand(name = "spawn", description = "Teleports to spawn", permission = "oakwood.spawn", usage = "/spawn", consoleAllowed = false)
public class SpawnCommand extends TeleportCommand
{
    public SpawnCommand(OakwoodPlugin plugin)
    {
        super(plugin, 30, 5);
    }

    @Override
    Location getTeleportLocation(Player player)
    {
        return plugin.getServer().getWorlds().get(0).getSpawnLocation();
    }
}
