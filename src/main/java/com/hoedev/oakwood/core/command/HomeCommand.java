package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import com.hoedev.oakwood.core.exception.UsageException;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import static com.hoedev.oakwood.core.service.PlayerDataService.toLocation;

@OakwoodCommand(name = "home", description = "Teleports home", permission = "oakwood.home", usage = "/home", consoleAllowed = false)
public class HomeCommand extends TeleportCommand
{
    public HomeCommand(OakwoodPlugin plugin)
    {
        super(plugin, 30, 5);
    }

    @Override
    Location getTeleportLocation(Player player) throws Exception
    {
        Location homeLocation = toLocation(plugin.getPlayerDataService().get(player.getUniqueId()).getHome());
        if (homeLocation == null)
        {
            throw new UsageException("You must set a home first to use this command");
        }
        return homeLocation;
    }
}
