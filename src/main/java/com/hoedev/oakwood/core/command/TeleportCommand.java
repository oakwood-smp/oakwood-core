package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.exception.UsageException;
import com.hoedev.oakwood.core.util.ExpiringSet;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.hoedev.oakwood.core.util.FormatUtils.formatMillis;

public abstract class TeleportCommand extends BaseCommand
{
    private final ExpiringSet<UUID> uuidExpiringSet;
    private final int warmupSeconds;

    public TeleportCommand(OakwoodPlugin plugin, int cooldownMinutes, int warmupSeconds)
    {
        super(plugin);
        this.uuidExpiringSet = new ExpiringSet<>(TimeUnit.MINUTES.toMillis(cooldownMinutes));
        this.warmupSeconds = warmupSeconds;
    }

    @Override
    void onCommand(CommandSender sender, Command command, String... args) throws Exception
    {
        Player player = (Player) sender;
        Location location = getTeleportLocation(player);
        if (uuidExpiringSet.contains(player.getUniqueId()))
        {
            long expirationMillis = uuidExpiringSet.getExpirationMillis(player.getUniqueId());
            throw new UsageException("You need to wait " + formatMillis(expirationMillis) + " before using this command again");
        }
        else
        {
            plugin.getChatService().send(sender, "Teleporting in 5 seconds... Don't move!");
            Location startLocation = player.getLocation();

            new BukkitRunnable()
            {
                private int checks = warmupSeconds * 2;

                @Override
                public void run()
                {
                    if (player.getLocation().distance(startLocation) > 1)
                    {
                        plugin.getChatService().sendError(sender, "You moved! Teleportation cancelled");
                        this.cancel();
                    }
                    if (checks-- == 0)
                    {
                        player.teleport(location);
                        uuidExpiringSet.add(player.getUniqueId());
                        plugin.getChatService().sendSuccess(sender, "Teleported successfully");
                        this.cancel();
                    }
                }
            }.runTaskTimer(plugin, 10, 10);
        }
    }

    abstract Location getTeleportLocation(Player player) throws Exception;
}
