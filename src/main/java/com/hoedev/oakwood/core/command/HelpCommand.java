package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.reflections.Reflections;

import java.util.Arrays;

@OakwoodCommand(name = "help", description = "Displays help", permission = "oakwood.help", usage = "/help")
public class HelpCommand extends BaseCommand
{
    private final OakwoodCommand[] commands;

    public HelpCommand(OakwoodPlugin plugin)
    {
        super(plugin);
        this.commands = new Reflections("com.hoedev.oakwood.core.command").getTypesAnnotatedWith(OakwoodCommand.class).stream()
                .map(c -> c.getAnnotation(OakwoodCommand.class))
                .toArray(OakwoodCommand[]::new);
    }

    @Override
    void onCommand(CommandSender sender, Command command, String... args)
    {
        plugin.getChatService().sendRaw(sender, ChatColor.GOLD + "Oakwood SMP Help:");
        plugin.getChatService().sendRaw(sender, "");
        Arrays.stream(commands)
                .filter(c -> sender.hasPermission(c.permission()))
                .forEach(c -> plugin.getChatService().sendRaw(sender, "%s%s%s -  %s", ChatColor.GOLD, c.usage(), ChatColor.GRAY, c.description()));
    }
}
