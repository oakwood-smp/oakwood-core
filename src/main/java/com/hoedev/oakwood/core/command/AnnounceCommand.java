package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@OakwoodCommand(name = "announce", description = "Broadcasts a message to the server", permission = "oakwood.announce", usage = "/announce")
public class AnnounceCommand extends BaseCommand
{
	public AnnounceCommand(OakwoodPlugin plugin)
	{
		super(plugin);
	}

	@Override
	void onCommand(CommandSender sender, Command command, String... args)
	{
		plugin.getChatService().broadcast(ChatColor.translateAlternateColorCodes('&', String.join(" ", args)));
	}

	@Override
	Collection<String> validateArguments(String... args)
	{
		return args.length == 0 ? Collections.singleton("You must provide a message") : Collections.emptySet();
	}
}
