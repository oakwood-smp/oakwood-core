package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import com.hoedev.oakwood.core.exception.OakwoodException;
import com.hoedev.oakwood.core.exception.UsageException;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.logging.Level;

public abstract class BaseCommand implements CommandExecutor
{
    final OakwoodPlugin plugin;

    private boolean consoleAllowed;
    private String successMessage;

    BaseCommand(OakwoodPlugin plugin)
    {
        this.plugin = plugin;
        initCommand();
    }

    private void initCommand()
    {
        OakwoodCommand command = this.getClass().getAnnotation(OakwoodCommand.class);
        this.consoleAllowed = command.consoleAllowed();
        this.successMessage = command.successMessage();
        Objects.requireNonNull(plugin.getCommand(command.name())).setExecutor(this);
        plugin.getLogger().info("Registered command " + command.name());
    }

    @Override
    public final boolean onCommand(CommandSender sender, Command command, String label, String... args)
    {
        try
        {
            Collection<String> usageErrors = validateUsage(sender, args);
            if (!usageErrors.isEmpty())
            {
                throw new UsageException(usageErrors);
            }
            onCommand(sender, command, args);
            if (StringUtils.isNotBlank(successMessage))
            {
                plugin.getChatService().sendSuccess(sender, successMessage);
            }
        }
        catch (Exception exception)
        {
            handleException(sender, exception);
        }
        return true;
    }

    abstract void onCommand(CommandSender sender, Command command, String... args) throws Exception;

    private Collection<String> validateUsage(CommandSender sender, String... args)
    {
        Collection<String> usageErrors = new HashSet<>();
        usageErrors.addAll(validateSender(sender));
        usageErrors.addAll(validateArguments(args));
        return usageErrors;
    }

    private Collection<String> validateSender(CommandSender sender)
    {
        return consoleAllowed || sender instanceof Player ? Collections.emptySet() : Collections.singleton("Cannot be executed from console");
    }

    Collection<String> validateArguments(String... args)
    {
        return args.length == 0 ? Collections.emptySet() : Collections.singleton("No arguments should be supplied");
    }

    void handleException(CommandSender sender, Exception exception)
    {
        if (exception instanceof OakwoodException)
        {
            if (exception instanceof UsageException)
            {
                ((UsageException) exception).getUsageErrors().forEach(usageError -> plugin.getChatService().sendError(sender, usageError));
            }
            else
            {
                plugin.getChatService().sendError(sender, exception.getMessage());
            }
        }
        else
        {
            plugin.getLogger().log(Level.WARNING, "Caught unexpected exception at " + this.getClass().getSimpleName(), exception);
            plugin.getChatService().sendError(sender, "Unknown error occured. Contact a member of staff");
        }
    }
}
