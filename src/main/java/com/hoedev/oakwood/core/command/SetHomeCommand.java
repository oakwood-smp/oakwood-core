package com.hoedev.oakwood.core.command;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import com.hoedev.oakwood.core.model.PlayerData;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.hoedev.oakwood.core.service.PlayerDataService.fromLocation;

@OakwoodCommand(name = "sethome", description = "Sets home", permission = "oakwood.home.set", usage = "/sethome", consoleAllowed = false, successMessage =
        "Home set successfully")
public class SetHomeCommand extends BaseCommand
{
    public SetHomeCommand(OakwoodPlugin plugin)
    {
        super(plugin);
    }

    @Override
    void onCommand(CommandSender sender, Command command, String... args) throws Exception
    {
        Player player = (Player) sender;
        PlayerData playerData = plugin.getPlayerDataService().get(player.getUniqueId());
        playerData.setHome(fromLocation(player.getLocation()));
        plugin.getPlayerDataService().save(playerData);
    }
}
