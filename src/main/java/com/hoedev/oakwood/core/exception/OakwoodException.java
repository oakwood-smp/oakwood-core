package com.hoedev.oakwood.core.exception;

public class OakwoodException extends Exception
{
    OakwoodException(String message)
    {
        super(message);
    }
}
