package com.hoedev.oakwood.core.exception;

import java.util.Collection;
import java.util.Collections;

public class UsageException extends OakwoodException
{
    private Collection<String> usageErrors;

    public UsageException(Collection<String> usageErrors)
    {
        super("Invalid command usage");
        this.usageErrors = usageErrors;
    }

    public UsageException(String usageError)
    {
        super("Invalid command usage");
        this.usageErrors = Collections.singleton(usageError);
    }

    public Collection<String> getUsageErrors()
    {
        return usageErrors;
    }
}
