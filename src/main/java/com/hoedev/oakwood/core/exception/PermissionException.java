package com.hoedev.oakwood.core.exception;

public class PermissionException extends OakwoodException
{
    public PermissionException()
    {
        super("Insufficient permissions");
    }
}
