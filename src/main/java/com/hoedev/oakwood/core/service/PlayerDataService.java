package com.hoedev.oakwood.core.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hoedev.oakwood.core.model.LocationData;
import com.hoedev.oakwood.core.model.PlayerData;
import com.hoedev.oakwood.core.model.Skill;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class PlayerDataService
{
    private static final Gson gson = new GsonBuilder().serializeNulls().create();

    private final Map<UUID, PlayerData> playerDataCache = new HashMap<>();
    private final File dataFolder;

    public PlayerDataService(File pluginDataFolder)
    {
        dataFolder = new File(pluginDataFolder, "player_data");
        dataFolder.mkdirs();
    }

    public PlayerData get(UUID uniqueId) throws FileNotFoundException
    {
        if (playerDataCache.containsKey(uniqueId))
        {
            return playerDataCache.get(uniqueId);
        }
        load(uniqueId);
        return get(uniqueId);
    }

    public void create(UUID uuid) throws IOException
    {
        PlayerData playerData = new PlayerData();
        playerData.setUniqueId(uuid);
        save(playerData);
        playerDataCache.put(uuid, playerData);
    }

    public void save(PlayerData playerData) throws IOException
    {
        File file = new File(dataFolder, playerData.getUniqueId() + ".json");
        if (!file.exists())
        {
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file);
        gson.toJson(playerData, fileWriter);
        fileWriter.flush();
        fileWriter.close();
    }

    public boolean exists(UUID uniqueId)
    {
        return new File(dataFolder, uniqueId + ".json").isFile();
    }

    public void load(UUID uniqueId) throws FileNotFoundException
    {
        playerDataCache.put(uniqueId, gson.fromJson(new FileReader(new File(dataFolder, uniqueId + ".json")), PlayerData.class));
    }

    public void unload(UUID uniqueId)
    {
        playerDataCache.remove(uniqueId);
    }

    public void addExperience(Player player, Skill skill, int amount) throws IOException
    {
        PlayerData playerData = get(player.getUniqueId());
        Map<Skill, Integer> skills = playerData.getSkills();
        skills.put(skill, skills.containsKey(skill) ? skills.get(skill) + amount : amount);
        save(playerData);
    }

    public static Location toLocation(LocationData loc)
    {
        return loc == null ? null : new Location(Bukkit.getWorld(loc.getWorld()), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
    }

    public static LocationData fromLocation(Location loc)
    {
        LocationData locationData = new LocationData();
        locationData.setWorld(Objects.requireNonNull(loc.getWorld()).getName());
        locationData.setX(loc.getX());
        locationData.setY(loc.getY());
        locationData.setZ(loc.getZ());
        locationData.setYaw(loc.getYaw());
        locationData.setPitch(loc.getPitch());
        return locationData;
    }
}
