package com.hoedev.oakwood.core.service;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ChatService
{
    private static final String PLUGIN_PREFIX = ChatColor.GOLD + "[OW] " + ChatColor.GRAY;

    public void sendSuccess(CommandSender sender, String format, Object... args)
    {
        send(sender, ChatColor.GREEN + format, args);
    }

    public void sendError(CommandSender sender, String format, Object... args)
    {
        send(sender, ChatColor.RED + format, args);
    }

    public void send(CommandSender sender, String format, Object... args)
    {
        sendRaw(sender, PLUGIN_PREFIX + format, args);
    }

    public void sendRaw(CommandSender sender, String format, Object... args)
    {
        sender.sendMessage(String.format(format, args));
    }

    public void broadcast(String format, Object... args)
    {
        broadcastRaw(PLUGIN_PREFIX + format, args);
    }

    public void broadcastWithPermission(String format, String permission, Object... args)
    {
        broadcastWithPermissionRaw(PLUGIN_PREFIX + format, permission, args);
    }

    public void broadcastWithPermissionRaw(String format, String permission, Object... args)
    {
        Bukkit.broadcast(String.format(format, args), permission);
    }


    public void broadcastRaw(String format, Object... args)
    {
        Bukkit.getServer().broadcastMessage(String.format(format, args));
    }
}
