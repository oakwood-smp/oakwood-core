package com.hoedev.oakwood.core.util;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExpiringSet<E> implements Collection<E>
{
    private final Map<E, Instant> map = new HashMap<>();
    private final long expirationMillis;

    public ExpiringSet(long expirationMillis)
    {
        this.expirationMillis = expirationMillis;
    }

    private void removeExpired()
    {
        map.values().removeIf(t -> Instant.now().isAfter(t.plusMillis(expirationMillis)));
    }

    @Override
    public int size()
    {
        removeExpired();
        return map.size();
    }

    @Override
    public boolean isEmpty()
    {
        removeExpired();
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o)
    {
        removeExpired();
        return map.containsKey(o);
    }

    @Override
    public Iterator<E> iterator()
    {
        removeExpired();
        return map.keySet().iterator();
    }

    @Override
    public void forEach(Consumer<? super E> action)
    {
        removeExpired();
        map.keySet().forEach(action);
    }

    @Override
    public Object[] toArray()
    {
        removeExpired();
        return map.keySet().toArray();
    }

    @Override
    public <T> T[] toArray(T[] t)
    {
        removeExpired();
        return map.keySet().toArray(t);
    }

    @Override
    public boolean add(E e)
    {
        map.put(e, Instant.now());
        return true;
    }

    @Override
    public boolean remove(Object o)
    {
        return map.remove(o) != null;
    }

    @Override
    public boolean containsAll(Collection<?> c)
    {
        removeExpired();
        return map.entrySet().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c)
    {
        map.putAll(c.stream().collect(Collectors.toMap(x -> x, x -> Instant.now())));
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c)
    {
        return map.keySet().removeAll(c);
    }

    @Override
    public boolean removeIf(Predicate<? super E> filter)
    {
        return map.keySet().removeIf(filter);
    }

    @Override
    public boolean retainAll(Collection<?> c)
    {
        return map.keySet().retainAll(c);
    }

    @Override
    public void clear()
    {
        map.clear();
    }

    @Override
    public Spliterator<E> spliterator()
    {
        removeExpired();
        return map.keySet().spliterator();
    }

    @Override
    public Stream<E> stream()
    {
        removeExpired();
        return map.keySet().stream();
    }

    @Override
    public Stream<E> parallelStream()
    {
        removeExpired();
        return map.keySet().parallelStream();
    }

    public long getExpirationMillis(E e)
    {
        return map.containsKey(e) ? map.get(e).toEpochMilli() + expirationMillis - System.currentTimeMillis() : 0;
    }
}
