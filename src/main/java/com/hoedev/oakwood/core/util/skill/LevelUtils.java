package com.hoedev.oakwood.core.util.skill;

import com.hoedev.oakwood.core.model.Skill;

import java.util.Map;

public class LevelUtils
{
    public static int getLevel(Map<Skill, Integer> skills, Skill skill)
    {
        return getLevel(skills.get(skill));
    }

    public static int getTotalLevel(Map<Skill, Integer> skills)
    {
        return getLevel(skills.values().stream().mapToInt(Integer::intValue).sum());
    }

    private static Integer getLevel(int xp)
    {
        return (int) Math.floor(1 + Math.sqrt(xp) * 0.1);
    }
}
