package com.hoedev.oakwood.core.util.skill;

import com.google.common.collect.ImmutableMap;
import org.bukkit.Material;

import java.util.Map;

public class MiningConstants
{
    public static final Map<Material, Integer> MATERIAL_XP = ImmutableMap.<Material, Integer>builder()
            .put(Material.OBSIDIAN, 1)
            .put(Material.COAL_ORE, 2)
            .put(Material.NETHER_QUARTZ_ORE, 2)
            .put(Material.IRON_ORE, 3)
            .put(Material.REDSTONE_ORE, 3)
            .put(Material.GOLD_ORE, 10)
            .put(Material.LAPIS_ORE, 5)
            .put(Material.DIAMOND_ORE, 20)
            .build();
}
