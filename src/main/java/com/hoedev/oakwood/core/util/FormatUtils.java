package com.hoedev.oakwood.core.util;

import java.util.concurrent.TimeUnit;

public class FormatUtils
{
    public static String formatMillis(long millis)
    {
        return TimeUnit.MILLISECONDS.toDays(millis) >= 1 ? TimeUnit.MILLISECONDS.toDays(millis) + "d" : (
                TimeUnit.MILLISECONDS.toHours(millis) >= 1 ? TimeUnit.MILLISECONDS.toHours(millis) + "h" : (
                        TimeUnit.MILLISECONDS.toMinutes(millis) >= 1 ? TimeUnit.MILLISECONDS.toMinutes(millis) + "m" :
                                TimeUnit.MILLISECONDS.toSeconds(millis) + "s"
                )
        );
    }

    public static String pluralise(String noun, int count)
    {
        return count == 1 ? noun : noun + "s";
    }
}