package com.hoedev.oakwood.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OakwoodCommand
{
    String name();

    String description() default "";

    String permission();

    String usage();

    boolean consoleAllowed() default true;

    String successMessage() default "";
}
