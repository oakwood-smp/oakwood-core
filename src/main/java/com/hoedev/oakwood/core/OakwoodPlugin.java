package com.hoedev.oakwood.core;

import com.hoedev.oakwood.core.annotation.OakwoodCommand;
import com.hoedev.oakwood.core.service.ChatService;
import com.hoedev.oakwood.core.service.PlayerDataService;
import net.coreprotect.CoreProtect;
import net.coreprotect.CoreProtectAPI;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.command.Commands;
import org.bukkit.plugin.java.annotation.permission.ChildPermission;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.permission.Permissions;
import org.bukkit.plugin.java.annotation.plugin.ApiVersion;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;
import org.bukkit.scheduler.BukkitRunnable;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.logging.Level;

@Plugin(name = "OakwoodCore", version = "1.0")
@Description("OakwoodSMP Core")
@Author("hoe")
@ApiVersion(ApiVersion.Target.v1_13)
@Commands({
        @Command(name = "spawn", desc = "Teleports to spawn", permission = "oakwood.spawn"),
        @Command(name = "help", desc = "Displays command info", permission = "oakwood.help"),
        @Command(name = "home", desc = "Teleports to home", permission = "oakwood.home"),
        @Command(name = "sethome", desc = "Sets home", permission = "oakwood.home.set"),
        @Command(name = "announce", desc = "Broadcasts a message to the server", permission = "oakwood.announce"),
})
@Permissions({
        @Permission(name = "oakwood.spawn", desc = "Grants access to the spawn command", defaultValue = PermissionDefault.TRUE),
        @Permission(name = "oakwood.help", desc = "Grants access to the help command", defaultValue = PermissionDefault.TRUE),
        @Permission(name = "oakwood.home", desc = "Grants access to the home command", defaultValue = PermissionDefault.TRUE, children =
                {@ChildPermission(name = "oakwood.home.set")}),
        @Permission(name = "oakwood.watch", desc = "Logs suspicious behaviour to users with this permission"),
        @Permission(name = "oakwood.skill.mining", desc = "Enables experience gain from mining", defaultValue = PermissionDefault.TRUE),
        @Permission(name = "oakwood.announce", desc = "Grants access to the announce command")
})
public class OakwoodPlugin extends JavaPlugin
{
    private final ChatService chatService = new ChatService();
    private final PlayerDataService playerDataService = new PlayerDataService(this.getDataFolder());
    private final CoreProtectAPI coreProtectAPI = ((CoreProtect) Objects.requireNonNull(getServer().getPluginManager().getPlugin("CoreProtect"))).getAPI();

    @Override
    public void onEnable()
    {
        try
        {
            initListeners();
            initCommands();
        }
        catch (Exception e)
        {
            getLogger().log(Level.SEVERE, "Fatal error encountered whilst enabling plugin", e);
            getServer().getPluginManager().disablePlugin(this);
        }
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                chatService.broadcast("Saving worlds...");
                getServer().getWorlds().forEach(World::save);
                chatService.broadcast(ChatColor.GREEN + "Worlds saved successfully!");
            }
        }.runTaskTimer(this, 12000, 36000);
    }

    private void initCommands() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        for (Class<?> clazz : new Reflections("com.hoedev.oakwood.core.command").getTypesAnnotatedWith(OakwoodCommand.class))
        {
            clazz.getConstructor(this.getClass()).newInstance(this);
        }
    }

    private void initListeners() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        for (Class<?> clazz : new Reflections("com.hoedev.oakwood.core.listener").getSubTypesOf(Listener.class))
        {
            if (!Modifier.isAbstract(clazz.getModifiers()))
            {
                clazz.getConstructor(this.getClass()).newInstance(this);
            }
        }
    }

    public ChatService getChatService()
    {
        return chatService;
    }

    public PlayerDataService getPlayerDataService()
    {
        return playerDataService;
    }

    public CoreProtectAPI getCoreProtectAPI()
    {
        return coreProtectAPI;
    }
}
