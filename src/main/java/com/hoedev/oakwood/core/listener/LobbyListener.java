package com.hoedev.oakwood.core.listener;

import com.hoedev.oakwood.core.OakwoodPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.IOException;
import java.util.UUID;

public class LobbyListener extends BaseListener
{
    public LobbyListener(OakwoodPlugin plugin)
    {
        super(plugin);
    }

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent joinEvent) throws IOException
    {
        UUID uniqueId = joinEvent.getPlayer().getUniqueId();
        if (plugin.getPlayerDataService().exists(uniqueId))
        {
            plugin.getPlayerDataService().load(joinEvent.getPlayer().getUniqueId());
        }
        else
        {
            plugin.getPlayerDataService().create(uniqueId);
        }
    }

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent quitEvent)
    {
        plugin.getPlayerDataService().unload(quitEvent.getPlayer().getUniqueId());
    }
}
