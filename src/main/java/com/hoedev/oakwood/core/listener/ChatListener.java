package com.hoedev.oakwood.core.listener;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.model.Skill;
import com.hoedev.oakwood.core.util.skill.LevelUtils;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.io.FileNotFoundException;
import java.util.Map;

public class ChatListener extends BaseListener
{
    public ChatListener(OakwoodPlugin plugin)
    {
        super(plugin);
    }

    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent chatEvent) throws FileNotFoundException
    {
        Map<Skill, Integer> skills = plugin.getPlayerDataService().get(chatEvent.getPlayer().getUniqueId()).getSkills();
        chatEvent.setFormat(ChatColor.GOLD + "[" + LevelUtils.getTotalLevel(skills) +  "] " + ChatColor.WHITE + "%s" + ChatColor.GOLD + " > " + ChatColor.RESET + "%s");
    }
}
