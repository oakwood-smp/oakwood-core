package com.hoedev.oakwood.core.listener;

import com.hoedev.oakwood.core.OakwoodPlugin;
import org.bukkit.event.Listener;

abstract class BaseListener implements Listener
{
    final OakwoodPlugin plugin;

    BaseListener(OakwoodPlugin plugin)
    {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
}
