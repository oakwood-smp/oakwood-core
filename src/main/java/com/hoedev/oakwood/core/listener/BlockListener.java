package com.hoedev.oakwood.core.listener;

import com.hoedev.oakwood.core.OakwoodPlugin;
import com.hoedev.oakwood.core.model.Skill;
import com.hoedev.oakwood.core.util.skill.MiningConstants;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;

import java.io.IOException;
import java.util.logging.Level;

public class BlockListener extends BaseListener
{
    public BlockListener(OakwoodPlugin plugin)
    {
        super(plugin);
    }

    @EventHandler
    private void onBreakEvent(BlockBreakEvent breakEvent)
    {
        Block block = breakEvent.getBlock();
        Player player = breakEvent.getPlayer();
        if (block.getType() == Material.DIAMOND_ORE)
        {
            Location location = block.getLocation();
            plugin.getChatService().broadcastWithPermission("%s found diamonds at %s, %s, %s", "oakwood.watch", player.getDisplayName(),
                    location.getBlockX(), location.getBlockY(), location.getBlockZ());
        }
        if(MiningConstants.MATERIAL_XP.containsKey(block.getType()) && player.hasPermission("oakwood.skill.mining"))
        {
            if(plugin.getCoreProtectAPI().blockLookup(block, 604800).size() == 0)
            {
                try
                {
                    plugin.getPlayerDataService().addExperience(player, Skill.MINING, MiningConstants.MATERIAL_XP.get(block.getType()));
                }
                catch (IOException e)
                {
                    plugin.getLogger().log(Level.SEVERE, "Caught IOException whilst attempting to write player data", e);
                }
            }
        }
    }
}
