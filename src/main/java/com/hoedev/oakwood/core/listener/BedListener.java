package com.hoedev.oakwood.core.listener;

import com.hoedev.oakwood.core.OakwoodPlugin;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import static com.hoedev.oakwood.core.util.FormatUtils.pluralise;
import static org.bukkit.event.player.PlayerBedEnterEvent.BedEnterResult.OK;

public class BedListener extends BaseListener
{
	private static final int SLEEP_PERCENTAGE = 50;

	private int sleeping = 0;

	public BedListener(OakwoodPlugin plugin)
	{
		super(plugin);
	}

	@EventHandler
	public void onBedEnterEvent(PlayerBedEnterEvent bedEnterEvent)
	{
		if (bedEnterEvent.getBedEnterResult() != OK || plugin.getServer().getOnlinePlayers().size() == 1)
		{
			return;
		}
		World defaultWorld = getDefaultWorld();
		int playerCount = getPlayersInWorld(defaultWorld);
		if (playerCount > 0)
		{
			int required = (int) Math.floor((SLEEP_PERCENTAGE * playerCount / 100.0) - ++sleeping);
			if (required <= 0)
			{
				plugin.getChatService().broadcast("%sEnough players are sleeping, skipping to day...", ChatColor.GREEN);
				long currentTime = defaultWorld.getTime();
				long timePerRun = (24000 - currentTime % 24000) / 30;

				new BukkitRunnable()
				{
					private int count = 1;
					@Override
					public void run()
					{
						if (count == 30)
						{
							defaultWorld.setTime(0); // Just to make sure it worked properly
							defaultWorld.setStorm(false);
							this.cancel();
							return;
						}
						defaultWorld.setTime(currentTime + (timePerRun * count++));
					}
				}.runTaskTimer(plugin, 0, 1);
			}
			else
			{
				plugin.getChatService().broadcast("%s%s is sleeping... only %s more %s needed to skip night",
						ChatColor.GRAY, bedEnterEvent.getPlayer().getDisplayName(), required, pluralise("player", required));
			}
		}
	}

	@EventHandler
	public void onBedLeaveEvent(PlayerBedLeaveEvent bedLeaveEvent)
	{
		// For some reason this event seems to get fired multiple times, so make sure we don't drop below 0
		if (sleeping >= 1)
		{
			sleeping--;
		}
	}

	private int getPlayersInWorld(World world)
	{
		return (int) plugin.getServer().getOnlinePlayers().stream().filter(player -> player.getWorld().equals(world)).count();
	}

	private World getDefaultWorld()
	{
		return plugin.getServer().getWorlds().get(0);
	}
}
