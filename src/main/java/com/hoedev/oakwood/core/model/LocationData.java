package com.hoedev.oakwood.core.model;

public class LocationData
{
	private String world;
	private Double x;
	private Double y;
	private Double z;
	private Float yaw;
	private Float pitch;

	public String getWorld()
	{
		return world;
	}

	public void setWorld(String world)
	{
		this.world = world;
	}

	public Double getX()
	{
		return x;
	}

	public void setX(Double x)
	{
		this.x = x;
	}

	public Double getY()
	{
		return y;
	}

	public void setY(Double y)
	{
		this.y = y;
	}

	public Double getZ()
	{
		return z;
	}

	public void setZ(Double z)
	{
		this.z = z;
	}

	public Float getYaw()
	{
		return yaw;
	}

	public void setYaw(Float yaw)
	{
		this.yaw = yaw;
	}

	public Float getPitch()
	{
		return pitch;
	}

	public void setPitch(Float pitch)
	{
		this.pitch = pitch;
	}
}
