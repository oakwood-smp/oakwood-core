package com.hoedev.oakwood.core.model;

public enum Skill
{
    MINING, FARMING, HUNTING, FISHING;
}
