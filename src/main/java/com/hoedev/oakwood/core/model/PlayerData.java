package com.hoedev.oakwood.core.model;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerData
{
	private UUID uniqueId;
	private LocationData home;
	private final Map<Skill, Integer> skills = new HashMap<>();

	public UUID getUniqueId()
	{
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId)
	{
		this.uniqueId = uniqueId;
	}

	public LocationData getHome()
	{
		return home;
	}

	public void setHome(LocationData home)
	{
		this.home = home;
	}

	public Map<Skill, Integer> getSkills()
	{
		return skills;
	}
}
